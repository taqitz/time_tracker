function doGet(e) {
    return HtmlService
      .createTemplateFromFile('INDEX')
      .evaluate().addMetaTag('viewport', 'width=device-width, initial-scale=1')
}

function include(filename) {
  return HtmlService.createHtmlOutputFromFile(filename)
    .getContent();
}


function project_data() {
  var ss = SpreadsheetApp.openByUrl('https://docs.google.com/spreadsheets/d/xxxxxxxxx/edit?usp=sharing'), // <-- REPLACE URL WITH YOUR SHEET'S URL (WHICH YOU GET FROM THE ADDRESS BAR OF YOUR WEB BROWSER OF THE OPEN GOOGLE SHEET) -->
    email = Session.getActiveUser().getEmail(),
    name = email.substring(0, (email.indexOf('@'))),
    projectData = ss.getSheetByName('projects'),
    accessData = ss.getSheetByName('project_access');

  var availCodes = getAccessData(accessData, name);

  var json = getProjectData(projectData, availCodes);

  return json;

}

function getName() {
  return Session.getActiveUser().getEmail();
}

function getAccessData(sheet, name) {
  var colStartIndex = 1;
  var rowNum = 1;
  var firstRange = sheet.getRange(1, 1, 1, sheet.getLastColumn());
  var firstRowValues = firstRange.getValues();
  var titleColumns = firstRowValues[0];

  // after the second line(data)
  var lastRow = sheet.getLastRow();
  var rowValues = [];
  for (var rowIndex = 2; rowIndex <= lastRow; rowIndex++) {
    var colStartIndex = 1;
    var rowNum = 1;
    var range = sheet.getRange(rowIndex, colStartIndex, rowNum, sheet.getLastColumn());
    var values = range.getValues();

    if (values[0][1] == name) {
      rowValues.push(values[0][0]);
    }
  }

  return rowValues;
}

function getProjectData(sheet, codes) {
  // first line(title)
  var colStartIndex = 1;
  var rowNum = 1;
  var firstRange = sheet.getRange(1, 1, 1, sheet.getLastColumn());
  var firstRowValues = firstRange.getValues();
  var titleColumns = firstRowValues[0];

  // after the second line(data)
  var lastRow = sheet.getLastRow();
  var rowValues = [];
  for (var rowIndex = 2; rowIndex <= lastRow; rowIndex++) {
    var colStartIndex = 1;
    var rowNum = 1;
    var range = sheet.getRange(rowIndex, colStartIndex, rowNum, sheet.getLastColumn());
    var values = range.getValues();

    if (values[0][4] == 'Active' && (values[0][1] == 1 || codes.indexOf(values[0][0]) >= 0)) {
      rowValues.push(values[0]);
    }

    rowValues.sort(function(x, y) {
      var xp = x[0];
      var yp = y[0];
      return xp == yp ? 0 : xp < yp ? 1 : -1;
    });

  }
  // create json
  var jsonArray = [];
  for (var i = 0; i < rowValues.length; i++) {
    var line = rowValues[i];
    var json = new Object();
    for (var j = 0; j < titleColumns.length; j++) {
      json[titleColumns[j]] = line[j];
    }
    jsonArray.push(json);
  }
  return jsonArray;
}
