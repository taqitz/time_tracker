function START() {


  var url = ""; // <-- insert your SLACK webhood URL here

  var payload = {
    "channel": "#everybody", // <-- DELETE IF YOU WANT TO USE DEFAULT CHANNEL; OTHERWISE CHANGE CHANNEL NAME
    "text" : "Please take 30 seconds and enter your time for today: https://script.google.com/", // <-- REPLACE THE https://script.google.com/ INSIDE THE DOUBLE QUOTES AT LEFT WITH YOUR SCRIPT LINK
    "icon_emoji": ":clock1:",
  }
  var nowH = new Date().getHours();
  var nowD = new Date().getDay();
  Logger.log('day : ' + nowD + '   Hours : ' + nowH)
  if (nowD == 6 || nowD == 0) {
    return
  }
  sendToSlack_(url, payload);
}


function sendToSlack_(url, payload) {
  var options = {
    "method": "post",
    "contentType": "application/json",
    "payload": JSON.stringify(payload)
  };
  return UrlFetchApp.fetch(url, options)
}
