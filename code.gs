function doGet(e) {

  var op = e.parameter.action;

  var ss = SpreadsheetApp.openById(SpreadsheetApp.getActiveSpreadsheet().getId());
  var sheet = ss.getSheetByName(e.parameter.sheet);

  if (op == "insert")
    return insert_value(e, sheet);

}

function insert_value(request, sheet) {
  var date = request.parameter.date;
  var proj = request.parameter.proj.split(",");
  var hours = request.parameter.hours.split(",");
  var lr = sheet.getLastRow();

  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1; //January is 0!
  var yyyy = today.getFullYear();

  if (dd < 10) {
    dd = '0' + dd
  }

  if (mm < 10) {
    mm = '0' + mm
  }

  today = mm + '/' + dd + '/' + yyyy;

  //add new row with recieved parameter from client
  var d = new Date();
  var currentTime = d.toLocaleString();

  if (date != today) {
    for (b = 2; b <= sheet.getLastRow(); b++) {
      var cDate = sheet.getRange(b, 2).getValue();
      var nDate = new Date();
      var dd1 = cDate.getDate();
      var mm1 = cDate.getMonth() + 1; //January is 0!
      var yyyy1 = cDate.getFullYear();

      if (dd1 < 10) {
        dd1 = '0' + dd1
      }

      if (mm1 < 10) {
        mm1 = '0' + mm1
      }

      nDate = mm1 + '/' + dd1 + '/' + yyyy1;

      if (nDate == date) {

        sheet.getRange(b, 5).setValue('inactive');
      }
    }
  }

  for (a = 0; a <= proj.length - 1; a++) {
    var rowData = sheet.appendRow([currentTime, date, proj[a], hours[a], 'active']);
  }
  var result = "Thanks for recording your time!";

  result = JSON.stringify({
    "result": result
  });

  return ContentService
    .createTextOutput(request.parameter.callback + "(" + result + ")")
    .setMimeType(ContentService.MimeType.JAVASCRIPT);
}


function read_value(request, ss, sheet) {
  var output = ContentService.createTextOutput(),
    data = {};

  data.records = readData_(ss, sheet);

  var callback = request.parameters.callback;

  if (callback === undefined) {
    output.setContent(JSON.stringify(data));
  } else {
    output.setContent(callback + "(" + JSON.stringify(data) + ")");
  }

  output.setMimeType(ContentService.MimeType.JAVASCRIPT);

  return output;
}


function readData_(ss, sheetname, properties) {

  if (typeof properties == "undefined") {
    properties = getHeaderRow_(ss, sheetname);
    properties = properties.map(function(p) {
      return p.replace(/\s+/g, '_');
    });
  }

  var rows = getDataRows_(ss, sheetname),
    data = [];

  for (var r = 0, l = rows.length; r < l; r++) {
    var row = rows[r],
      record = {};

    for (var p in properties) {
      record[properties[p]] = row[p];
    }

    data.push(record);

  }
  return data;
}



function getDataRows_(ss, sheetname) {
  var sh = ss.getSheetByName(sheetname);

  return sh.getRange(2, 1, sh.getLastRow() - 1, sh.getLastColumn()).getValues();
}


function getHeaderRow_(ss, sheetname) {
  var sh = ss.getSheetByName(sheetname);

  return sh.getRange(1, 1, 1, sh.getLastColumn()).getValues()[0];
}
